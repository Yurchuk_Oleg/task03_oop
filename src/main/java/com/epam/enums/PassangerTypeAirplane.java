package com.epam.enums;

public enum PassangerTypeAirplane {
    wideBody,
    narrowBody,
    regional,
    commuterLiners
}
