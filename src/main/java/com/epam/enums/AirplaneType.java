package com.epam.enums;

public enum AirplaneType {
    Small,
    Medium,
    Large
}
