package com.epam.content;

import com.epam.enums.AirplaneType;
import com.epam.enums.PassangerTypeAirplane;

public class PassengerAirplane extends Airplane {
    private final PassangerTypeAirplane passangerTypeAirplane;

    public PassengerAirplane(String name, int numberOfSeats,
                             int carryingCapacity, int numberOfEngines,
                             int maxFlightRange, int litersPerHour,
                             int fuelReserve, AirplaneType airplaneType,
                             PassangerTypeAirplane passangerTypeAirplane) {
        super(name, numberOfSeats,
                carryingCapacity, numberOfEngines,
                maxFlightRange, litersPerHour,
                fuelReserve, airplaneType);
        this.passangerTypeAirplane = passangerTypeAirplane;
    }

    public PassangerTypeAirplane getPassangerTypeAirplane() {
        return passangerTypeAirplane;
    }

    @Override
    public String getInfo() {
        return "Name: " + getName() + "\nNum of Seats: " + getNumberOfSeats()
                + "\nCarrying capacity: " + getCarryingCapacity()
                + "\nNum of engines: " + getNumberOfEngines()
                + "\nMax flight range: " + getMaxFlightRange() + "(km)"
                + "\nLiters per hour: " + getLitersPerHour()
                + "\nFuel Reserve: " + getFuelReserve() + "(liters)"
                + "\nAirplane type: " + getAirplaneType()
                + "\nPassanger type: " + getPassangerTypeAirplane();
    }
}
