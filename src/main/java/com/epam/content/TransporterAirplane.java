package com.epam.content;

import com.epam.enums.AirplaneType;

public class TransporterAirplane extends Airplane {
    final boolean itemsMechanizationOfLoading;
    final boolean itemsMechanizationOfUnloading;

    public TransporterAirplane(String name, int numberOfSeats,
                               int carryingCapacity, int numberOfEngines,
                               int maxFlightRange, int litersPerHour,
                               int fuelReserve, AirplaneType airplaneType,
                               boolean itemsMechanizationOfLoading, boolean itemsMechanizationOfUnloading) {
        super(name, numberOfSeats,
                carryingCapacity, numberOfEngines,
                maxFlightRange, litersPerHour,
                fuelReserve, airplaneType);
        this.itemsMechanizationOfLoading = itemsMechanizationOfLoading;
        this.itemsMechanizationOfUnloading = itemsMechanizationOfUnloading;
    }

    public boolean isItemsMechanizationOfLoading() {
        return itemsMechanizationOfLoading;
    }

    public boolean isItemsMechanizationOfUnloading() {
        return itemsMechanizationOfUnloading;
    }

    @Override
    public String getInfo() {
        return "Name: " + getName() + "\nNum of Seats: " + getNumberOfSeats()
                + "\nCarrying capacity: " + getCarryingCapacity()
                + "\nNum of engines: " + getNumberOfEngines()
                + "\nMax flight range: " + getMaxFlightRange() + "(km)"
                + "\nLiters per hour: " + getLitersPerHour()
                + "\nFuel Reserve: " + getFuelReserve() + "(liters)"
                + "\nAirplane type: " + getAirplaneType()
                + "\nItemsMechanizationOfLoading: " + isItemsMechanizationOfLoading()
                + "\nitemsMechanizationOfUnloading: " + isItemsMechanizationOfUnloading();
    }
}

