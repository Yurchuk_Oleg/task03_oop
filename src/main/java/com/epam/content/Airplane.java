package com.epam.content;

import com.epam.enums.AirplaneType;

public abstract class Airplane {
    private final String name;
    private final int numberOfSeats;
    private final int carryingCapacity;
    private final int numberOfEngines;
    private final int maxFlightRange;
    private final int litersPerHour;
    private final int fuelReserve;
    private final AirplaneType airplaneType;

    public Airplane(String name, int numberOfSeats,
                    int carryingCapacity, int numberOfEngines,
                    int maxFlightRange, int litersPerHour,
                    int fuelReserve, AirplaneType airplaneType) {
        this.name = name;
        this.numberOfSeats = numberOfSeats;
        this.carryingCapacity = carryingCapacity;
        this.numberOfEngines = numberOfEngines;
        this.maxFlightRange = maxFlightRange;
        this.litersPerHour = litersPerHour;
        this.fuelReserve = fuelReserve;
        this.airplaneType = airplaneType;
    }

    public abstract String getInfo();

    public String getName() {
        return name;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public int getCarryingCapacity() {
        return carryingCapacity;
    }

    public int getNumberOfEngines() {
        return numberOfEngines;
    }

    public int getMaxFlightRange() {
        return maxFlightRange;
    }

    public int getLitersPerHour() {
        return litersPerHour;
    }

    public int getFuelReserve() {
        return fuelReserve;
    }

    public AirplaneType getAirplaneType() {
        return airplaneType;
    }
}
