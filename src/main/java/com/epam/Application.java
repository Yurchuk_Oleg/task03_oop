/**
 * The package com.epam where is main point of program.
 */
package com.epam;

import com.epam.view.AirportView;

/**
 * This class in main.
 */
public class Application {
    /**
     * This is a start point of program.
     *
     * @param args - arguments.
     */
    public static void main(String[] args) {
        new AirportView().show();
    }
}
