package com.epam.controller;

import com.epam.model.AirportModel;
import com.epam.model.BussinesLogic;

public class AirportControllerImpl implements AirportController {
    private AirportModel airportModel;

    public AirportControllerImpl() {
        airportModel = new BussinesLogic();
    }

    @Override
    public void displaySortAirplanesByMaxFlightRange() {
        airportModel.displaySortAirplanesByMaxFlightRange();
    }

    @Override
    public void getInfo() {
        airportModel.getInfo();
    }

    @Override
    public void displayTotalNumbersOfSeats() {
        airportModel.displayTotalNumbersOfSeats();
    }

    @Override
    public void displayTotalCapacity() {
        airportModel.displayTotalCapacity();
    }

    @Override
    public void displayAirplanesByLitersPerHour(int from, int to) {
        airportModel.displayAirplanesByLitersPerHour(from, to);
    }
}
