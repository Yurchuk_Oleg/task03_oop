package com.epam.controller;

public interface AirportController {
    void displaySortAirplanesByMaxFlightRange();

    void displayTotalNumbersOfSeats();

    void getInfo();

    void displayTotalCapacity();

    void displayAirplanesByLitersPerHour(int from, int to);
}
