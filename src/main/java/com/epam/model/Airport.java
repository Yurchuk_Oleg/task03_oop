package com.epam.model;

import com.epam.content.Airplane;
import com.epam.content.PassengerAirplane;
import com.epam.content.TransporterAirplane;
import com.epam.enums.AirplaneType;
import com.epam.enums.PassangerTypeAirplane;

import java.util.*;
import java.util.stream.Collectors;

public class Airport {
    private List<Airplane> airplanes;

    Airport() {
        airplanes = new ArrayList<>();
        airplanes.add(new PassengerAirplane("Boeing 777", 400, 160000,
                2, 15000, 6000, 180000,
                AirplaneType.Large, PassangerTypeAirplane.wideBody));
        airplanes.add(new PassengerAirplane("Fokker 50", 58, 8000,
                2, 5000, 600, 6000,
                AirplaneType.Small, PassangerTypeAirplane.commuterLiners));
        airplanes.add(new TransporterAirplane("Ан-225", 12, 300000,
                6, 14000, 16000, 300000,
                AirplaneType.Large, true, true));
        airplanes.add(new TransporterAirplane("MD-11F", 10, 120000,
                4, 10000, 4000, 100000,
                AirplaneType.Medium, true, true));
    }

    private List<Airplane> sortAirplanesByMaxFlightRange(List<Airplane> airplaneList) {
        airplaneList.sort(Comparator.comparingInt(Airplane::getMaxFlightRange));
        return airplaneList;
    }

    public void displaySortAirplanesByMaxFlightRange() {
        List<Airplane> sortedList = sortAirplanesByMaxFlightRange(airplanes);
        for (Airplane airplane : sortedList) {
            System.out.println(airplane.getInfo());
        }
    }

    public void displayTotalNumbersOfSeats() {
        int amountNumOfSeats = 0;
        for (Airplane airplane : airplanes) {
            amountNumOfSeats += airplane.getNumberOfSeats();
        }
        System.out.println("The total number of seats: " + amountNumOfSeats);
    }

    public void getInfo() {
        ListIterator<Airplane> iterator = airplanes.listIterator();
        while (iterator.hasNext()) {
            int i = iterator.nextIndex();
            System.out.println("\n" + i + ". " + iterator.next().getInfo());
        }
    }

    public void displayTotalCapacity() {
        int totalCapacity = 0;
        for (Airplane airplane : airplanes) {
            totalCapacity += airplane.getCarryingCapacity();
        }
        System.out.println("The total capacity: " + totalCapacity + " (kg)");
    }

    public void displayAirplanesByLitersPerHour(int from, int to) {
        List<Airplane> airplanes = findByLitersPerHourBetween(from, to);
        for (Airplane airplane : airplanes) {
            System.out.println(airplane.getInfo());
        }
    }

    private List<Airplane> findByLitersPerHourBetween(int from, int to) {
        return airplanes.stream()
                .filter(airplane -> airplane.getLitersPerHour() >= from)
                .filter(airplane -> airplane.getLitersPerHour() <= to)
                .collect(Collectors.toList());
    }

}
