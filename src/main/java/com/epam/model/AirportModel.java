package com.epam.model;

public interface AirportModel {
    void displaySortAirplanesByMaxFlightRange();

    void displayTotalNumbersOfSeats();

    void displayTotalCapacity();

    void getInfo();

    void displayAirplanesByLitersPerHour(int from, int to);
}
