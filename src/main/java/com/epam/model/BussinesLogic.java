package com.epam.model;

public class BussinesLogic implements AirportModel {
    private Airport airport;

    public BussinesLogic() {
        airport = new Airport();
    }

    @Override
    public void displaySortAirplanesByMaxFlightRange() {
        airport.displaySortAirplanesByMaxFlightRange();
    }

    @Override
    public void getInfo() {
        airport.getInfo();
    }

    @Override
    public void displayTotalNumbersOfSeats() {
        airport.displayTotalNumbersOfSeats();
    }

    @Override
    public void displayTotalCapacity() {
        airport.displayTotalCapacity();
    }

    @Override
    public void displayAirplanesByLitersPerHour(int from, int to) {
        airport.displayAirplanesByLitersPerHour(from, to);
    }
}
