package com.epam.view;

import com.epam.controller.AirportController;
import com.epam.controller.AirportControllerImpl;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class AirportView {
    private AirportController airportController;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public AirportView() {
        airportController = new AirportControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - Display AirplaneList");
        menu.put("2", "  2 - Sort AirplaneList by max flight range");
        menu.put("3", "  3 - Display total numbers of seats");
        menu.put("4", "  4 - Display total capacity");
        menu.put("5", "  5 - Display airplanes " +
                "which fits a given range of fuel consumption options");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
    }

    private void pressButton1() {
        airportController.getInfo();
    }

    private void pressButton2() {
        airportController.displaySortAirplanesByMaxFlightRange();
    }

    private void pressButton3() {
        airportController.displayTotalNumbersOfSeats();
    }

    private void pressButton4() {
        airportController.displayTotalCapacity();
    }

    private void pressButton5() {
        System.out.println("Enter a parameter range...");
        System.out.println("Enter from: ");
        int from = Integer.parseInt(input.nextLine());
        System.out.println("Enter to: ");
        int to = Integer.parseInt(input.nextLine());
        airportController.displayAirplanesByLitersPerHour(from, to);
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception ignored) {
            }
        } while (!keyMenu.equals("Q"));
    }

}
